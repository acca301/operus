#!/usr/bin/env perl

use strict;
use utf8;
use 5.18.0;
use Time::HiRes;
use Opervs;
use Data::Dumper;
use POSIX;
use Fcntl qw(:flock SEEK_END);
use FindBin;

my ($lock_expire, $lock_try) = (2, 2);

my (
	$nforks, 
	$ncount,
	$to_sleep,
	$dumponly,
	$zeroing,
	$spare,
	);
$to_sleep = 0.5;

unless ( scalar(@ARGV) ) {
	say "Make concurrent testing for Opervs.pm";
	say "Usage: $0 <options>";
	say "Where <options> are:";
	say "\t-d = Dump stack only. Other parameters are ignored";
	say "\t-z = Zeroing stack only. Other parameters are ignored";
	say "\t-n <NN> = Number of concurrrent processes";
	say "\t-c <NN> = Number of iterations per process";
	say "\t-s <NN> = Iteration delay (sleep) timer. Default 0.5";
	exit 0;
}

push @ARGV, 1 if scalar(@ARGV) / 2 != int(scalar(@ARGV) / 2);

my %params = @ARGV;
while ( my( $par, $val) = each( %params) ) {			# Decompose param string
	$par =~ s/^-+//;
	if( lc($par) eq 'n' ) {
		$nforks = $val;
	} elsif( lc($par) eq 'c' ) {
		$ncount = $val;
	} elsif( lc($par) eq 's' ) {
		$to_sleep = $val;
	} elsif( lc($par) eq 'd' ) {
		$dumponly = 1;
	} elsif( lc($par) eq 'z' ) {
		$zeroing = 1;
	} else {
		$spare->{ lc($par)} = $par 
	}
}

my $logfile = "$FindBin::Bin/stack.log";
open( my $fh, "> $logfile");			# Zeroing previous log
my $msg = timestr();
say $fh "Parent process $$ started at $msg.";
say $fh "Forks: $nforks, Iterations: $ncount, Delay: $to_sleep, lock_expire: $lock_expire, lock_try: $lock_try";
say $fh  (' ' x 18). "Time\tPID\tUMode\tSize0\tSize1\tError\tMessage";
close( $fh);

if ( $dumponly || $zeroing ) {			# Service procedures
	my $qw = Opervs->new(
					name => 'OVS::',
					servers => ['localhost:11211'],
					max_stack => 50,
					lock_expire => $lock_expire,
					lock_try => $lock_try,
					expire_time => 3*60*60*24,
				);
	my $stack = $qw->get_stack;
	print Dumper( $stack);
	$qw->pop( scalar( @$stack)) if $zeroing;
	say $qw->error, ' ', $qw->message;
	exit 0;
}

my $exec = sub {		# Special executing action
		my $tokens = shift;
		my $ret = {};
		for my $word ( @$tokens) {
			my $done = "Done for $word";
			$ret->{$word} = $done;
			$word = $done;
		}
		return $ret;
	};

my @child;
for (1..$nforks) {
	print "Launch process #$_ ";
	my $cpid = fork();
	
	if ( $cpid ) {
		print "with pid $cpid\n";
		push( @child, $cpid);
		sleep( $to_sleep);
		next;
	}
	op_stack($ncount, $exec);
	exit;
}

say "Waiting for ", scalar(@child), " processes done...";
while( my $cpid = shift( @child) ) {
	do { $cpid = waitpid( $cpid, 0) } while $cpid > 0
}
say "All done!";

#################
sub op_stack {	#
#################
	my ($count, $action) = @_;
	my $qw = Opervs->new(
					name => 'OVS::',
					servers => ['localhost:11211'],
					max_stack => 50,
					lock_expire => $lock_expire,
					lock_try => $lock_try,
					expire_time => 3*60*60*24,
				);

	while ( $count-- ) {
		my $umode = sprintf('%0.0f', rand);		# 0 = user make pop, 1 = user make transactions
		my $msg = [ $$,
					$umode,
				];

		if ( $umode ) {
			my $params = [ "exec", $$, "loop$count"];
			my $res = $qw->transact($action, $params);
			push( @$msg, @{$qw->changes},		# Stack length after
						$qw->error,
						$qw->message);

		} else {
			my $nrows = sprintf('%0.0f', rand( $qw->changes->[1]) );
			my $slice = $qw->pop( $nrows);
			push( @$msg, @{$qw->changes},		# Stack length after
						$qw->error,
						$qw->message);
		}
		dump_log( $msg);
		sleep( $to_sleep);
	}
}

##################################
sub dump_log {
##################
	my $data = shift;
	my $record = $data;
	$record = join("\t", @$data) if ref( $data) eq 'ARRAY';
	$record = timestr()."\t$record\n";

	open( my $fh, ">> $logfile");
	flock( $fh, LOCK_EX );
	seek( $fh, 0, SEEK_END );
	print $fh $record;
	flock( $fh, LOCK_UN );
	close( $fh);
	return 1;
}
#################
sub timestr {					#  Make string from serial date number
#################
my ($datetime, $gmt) = @_;

	$datetime = $datetime || Time::HiRes::time();
	$datetime = sprintf( '%.2f', $datetime );

	my @date = localtime( $datetime );		#  Further need
	@date = gmtime( $datetime ) if $gmt;		#  GMT

	$date[6] = substr($datetime,index($datetime, '.')+1) if $datetime =~ /\./;

	$date[5]+=1900;
	$date[4]++;
	map { $date[$_] = sprintf('%02d', $date[$_]) } (0, 1, 2, 3, 4);

	return ($date[5], $date[4], $date[3],$date[2],$date[1],$date[0]) if wantarray();
	return "$date[3]-$date[4]-$date[5] $date[2]:$date[1]:$date[0].$date[6]";
}
